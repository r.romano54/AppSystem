//
//  AppSystemApp.swift
//  AppSystem
//
//  Created by Raffaella Romano on 20/06/22.
//

import SwiftUI

@main
struct AppSystemApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
